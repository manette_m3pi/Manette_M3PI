package com.example.i_p34lounet.manettem3pi;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * Classe permettant d'heberger une instance de la classe ControllerFragment
 *
 * @see android.app.Activity
 */

public class Controller extends AppCompatActivity {
    public static String TAG_FRAGMENT = "controller";

    /**
     * methode dans laquelle sont effectuees les initialisations de base
     *
     * @param savedInstanceState dernier etat de l'instance sauvegardee
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*gonfler une interface graphique pour l'instance actuelle de la classe Controller*/
        setContentView(R.layout.activity_main);
        /*demander au système d'exploitation de ne pas mettre l'ecran en veille tant que l'instance en cours est au premier plan */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        /*si on a declare qu'on demenderait la permission de manipuler l'adaptateur Bluetooth*/
        if (checkForPermissions(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED
                && checkForPermissions(Manifest.permission.BLUETOOTH_ADMIN)
                == PackageManager.PERMISSION_GRANTED) {
                    /*demander a l'utilisateur d'accorder a l'activite actuelle les droits sur l'adaptateur Bluetooth*/
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN}, ControllerFragment
                    .PERMISSION_REQUEST_CODE);
          /*si on ne l'a pas fait*/
        } else {
            /*dire qu'on a besoin de permissions*/
            Toast.makeText(this, "we need the permissions. Goodbye", Toast.LENGTH_SHORT).show();
			/*quitter*/
            finish();
        }

    }

    /**
     * methode qui verifie les droits qu'on a pour l'application
     *
     * @param permission le nom de la permission
     * @return le code de la reponse
     */
    private int checkForPermissions(String permission) {
        return ContextCompat.checkSelfPermission(this, permission);
    }

    /**
     * methode appelee après que l'utilisateur ait repondu a la demande de permissions
     *
     * @param requestCode  le meme code que celui envoye par l'acitivite
     * @param permissions  les noms des permissions
     * @param grantResults les valeurs des reponses de l'utilisateur
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		/*si le code de requete est le meme que celui utilise pour nos permissions*/
        if (requestCode == ControllerFragment.PERMISSION_REQUEST_CODE) {
			/*si l'utilisateur a donne son accord pour toutes les permissions requises*/
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				/*recuperer l'adaptateur Bluetooth*/
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
				/*si le telephone dispose d'une radio Bluetooth*/
                if (adapter != null) {
					/*si la radio n'est pas alimentee*/
                    if (!adapter.isEnabled()) {
						/*generer une "intention" de lancement d'une acitivite de demande de permissions*/
                        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						/*lancer une actiivite dans la quelle on demandera a l'utilisateur d'allumer la radio Bluetooth*/
                        startActivityForResult(enableIntent, ControllerFragment.ENABLE_REQUEST_CODE);
                        /*si la radio est alimentee*/
                    } else {
                        /*charger l'interface graphique du controleur*/
                        loadFragments();
                    }
                    /*si le telephone n'a pas de radio Bluetooth*/
                } else {
                    /*annoncer a l'utilisateur que son telephone ne dispose pas d'une radio
                    Bluetooth*/
                    Toast.makeText(this, "your device doesn't support Bluetooth. Goodbye",
                            Toast.LENGTH_SHORT).show();
                    /*fermer l'activite*/
                    finish();
                }
            }
        }
    }

    /**
     * methode appele au retour d'une activite lancee avec resultat d'execution attendu
     *
     * @param requestCode le meme code que celui envoyee par l'activite source
     * @param resultCode  le code du resultat retourne par l'activite cible
     * @param data        les donnee possibles que pourrait joindre l'application cible
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*si le code de requete correspond a celui que l'activite a envoye*/
        if (requestCode == ControllerFragment.ENABLE_REQUEST_CODE) {
            /*si le code du resultat est negatif*/
            if (resultCode != Activity.RESULT_OK) {
                /*dire a l'utilisateur qu'il doit activer la radio Bluetooth*/
                Toast.makeText(this, "we need you to enable bluetooth", Toast.LENGTH_SHORT).show();
                /*fermer l'instance actuelle de l'activite*/
                finish();
                /*si on a toutes les permissions*/
            } else
            /*charger l'interface graphique du controleur*/
                loadFragments();
        }
    }

    /**
     * methode qui permet de charger un Fragment auquel est attachee une interface graphique
     */
    private void loadFragments() {
        /*generer une nouvelle instance du ControllerFragment*/
        ControllerFragment controllerFragment = ControllerFragment.newInstance();
        /*recuperer une transaction*/
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        /*ajouter l'instance du Fragment generee a l'activite en lui attachant une interface
        graphique*/
        transaction.add(R.id.activity_main, controllerFragment, TAG_FRAGMENT);
        /*appliquer toutes les operations prevues par la transaction*/
        transaction.commit();
    }
}
