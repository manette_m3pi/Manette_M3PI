package com.example.i_p34lounet.manettem3pi;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by nassim on 27/12/2016.
 * classe qui implement un {@link Runnable}
 *
 * @see Runnable
 */

public class BatteryReader implements Runnable {
    /*titre a utiliser pour reconnaitre les messages de debogages associes a cette classe*/
    private static final String TAG = "BatteryReader";
    /*active ou desactive le mode de debogage*/
    private static final boolean DEBUG = true;
    /*reference vers un tunnel de communication Bluetooth RFCOMM*/
    private BluetoothSocket mSocket;
    /*reference vers une instance du ControllerFragment*/
    private ControllerFragment mContext;
    /*reference vers un flux en entree du tunnel de communication Bluetooth RFCOMM*/
    private InputStream mInputStream;
    /*reference vers un flux en sortie du tunnel de communication Bluetooth RFCOMM*/
    private OutputStream mOutputStream;
    /*tableau d'un octet contenant le code a envoyer pour demander la mise a jour de la valeur du
     niveau de la batterie du robot Pololu(TM) m3pi*/
    private byte[] mRequestBuffer = new byte[1];
    /*tablea d'un octet servant d'espace de stockage du niveau de la batterie remis par le robot
    Pololu(TM) m3pi*/
    private byte[] mResPonseBuffer = new byte[1];

    /**
     * constructeur
     *
     * @param context une instance du {@link ControllerFragment}
     * @see ControllerFragment
     */
    public BatteryReader(ControllerFragment context) {
        mContext = context;
        mSocket = mContext.getmSocket();
        try {
            mInputStream = mSocket.getInputStream();
            mOutputStream = mSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mRequestBuffer[0] = (byte) 0xee;
        if (DEBUG)
            Log.d(TAG, "Thread created");
    }

    /**
     * methode appele par une tache dont une instance de ce {@link Runnable} est attachee
     */
    @Override
    public void run() {
        /*tant que le declencheur d'arret n'est pas actif*/
        while (!mContext.ismStopTrigger()) {
            /*tant que l'application est connecte a la Raspberry Pi*/
            while (mContext.ismIsConnected()) {
                /*verouiller sur mOutputStream et bloquer toute autre tache qui verouille dessu*/
                synchronized (mOutputStream) {
                    /*essayer*/
                    try {
                        /*envoyer les valeur du tableau mRequestBuffer a la Raspberry Pi*/
                        mOutputStream.write(mRequestBuffer);
                        /*gerer les exceptions generees par le bloc d'essai*/
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (DEBUG)
                    Log.d(TAG, "Battery read request sent");
                try {
                    /*la tache se bloque jusqu'a ce qu'on recoive une reponse de la part de la
                    Raspberry
                    Pi*/
                    mInputStream.read(mResPonseBuffer);
                    /*mettre a jour le niveau de la batterie au niveau de l'interface graphique*/
                    updateContext();
                    /*dormir le temps de l'intervalle de rafraichissement du niveau de la batterie*/
                    Thread.sleep(mContext.getmBatteryCheckTime());
                    /*gerer les exceptions generees dans le bloc d'essai*/
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * methode qui met a jour la valeur du niveau de la batterie au niveau de l'interface
     * graphique geree par la tache qui lui est associee
     */
    private void updateContext() {
        /*faire executer ce code supplementaire par la tache proprietaire de l'inerface graphique*/
        mContext.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /*recuperer l'instance de l'Activity du contexte*/
                ControllerFragment controller = (ControllerFragment) mContext.getActivity()
                        .getFragmentManager()
                        .findFragmentByTag(Controller.TAG_FRAGMENT);
                /*changer la valeur du niveau de la batterie du robot Pololu(TM) m3pi*/
                controller.setmBatteryLevel((int) mResPonseBuffer[0]);
            }
        });
    }
}
