package com.example.i_p34lounet.manettem3pi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by nassim on 27/12/2016.
 * classe qui implement {@link Runnable}
 *
 * @see Runnable
 */

public class ConnectionEstablisher implements Runnable {
    /*active ou desactive le mode de debogage*/
    private static final boolean DEBUG = true;
    /*le titre a utiliser pour reconnaitre les textes de debogage relatifs a cette classe*/
    private static String TAG = "ConnectionEstablisher";
    /*reference vers l'instance du contexte*/
    private ControllerFragment mContext = null;
    /*reference vers le dispositif Bluetooth (ici la Raspberry Pi)*/
    private BluetoothDevice mDevice = null;
    /*reference vers le tunnel de communication Bluetooth RFCOMM*/
    private BluetoothSocket mSocket = null;

    /**
     * constructeur
     *
     * @param context le contexte. Doit etre une instance de la classe {@link ControllerFragment}
     * @see ControllerFragment
     */
    public ConnectionEstablisher(ControllerFragment context) {
        mContext = context;
        mDevice = mContext.getmDevice();
        if (DEBUG)
            Log.d(TAG, "Thread created");
    }

    /**
     * methode appelee par une tache dont ce Runnable est associee
     */
    @Override
    public void run() {
        /*recuperer la reference vers l'adaptateur Bluetooth de ce telephone*/
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        /*si la reference vers le dispositif Bluetooth (ici la Raspberry Pi) est valide*/
        if (mDevice != null) {
            /*essayer*/
            try {
                /*creer une reference de Methode*/
                Method method;
                /*preparer une methode a executer dont le nom correspond le plus a
                "createRfcommSocket"*/
                method = mDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                /*invoquer cette methode avec des parametres*/
                mSocket = (BluetoothSocket) method.invoke(mDevice, 1);
                /*connecter le tunnel de communication Bluetooth RFCOMM*/
                mSocket.connect();
                /*modifier la reference vers le tunnel de communication Bluetooth RFCOMM*/
                mContext.setmSocket(mSocket);
                /*annoncer qu'on est connecte*/
                mContext.setmIsConnected(true);
                if (DEBUG)
                    Log.d(TAG, "connection with RFCOMM socket established");
                mContext.setmIsConnected(true);
                /*gerer les erreurs a l'echec de l'essai*/
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            /*appeler une tache de commandes CommandWriter*/
            new Thread(new CommandWriter(mContext, 1, (byte) 0x1)).start();
            /*appeler une tache de commandes CommandWriter*/
            new Thread(new CommandWriter(mContext, 2, (byte) 0x2)).start();
            /*appeler une tache de commandes CommandWriter*/
            new Thread(new CommandWriter(mContext, 3, (byte) 0x3)).start();
            /*appeler une tache de commandes CommandWriter*/
            new Thread(new CommandWriter(mContext, 4, (byte) 0x4)).start();
            /*appeler une tache de commandes CommandWriter*/
            new Thread(new BatteryReader(mContext)).start();
            if (DEBUG)
                Log.d(TAG, "Leaving connection Thread");
        }
    }
}
